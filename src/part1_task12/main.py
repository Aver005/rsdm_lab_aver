import random


def fill_array(size, min_value, max_value):
    """
    Заполняет массив случайными значениями.

    :param size: Размер массива (количество элементов)
    :param min_value: Минимальное значение элемента массива
    :param max_value: Максимальное значение элемента массива
    :return: Сгенерированный массив
    """
    return [random.randint(min_value, max_value) for _ in range(size)]


def print_array(arr):
    """
    Выводит массив на экран.

    :param arr: Массив, который необходимо вывести
    """
    print(arr)


# Пример использования функций:
array_size = 10
min_value = 1
max_value = 100

# Заполнение массива случайными значениями
my_array = fill_array(array_size, min_value, max_value)

# Вывод массива на экран
print_array(my_array)
